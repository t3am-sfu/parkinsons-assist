/*
 SelectActivityType.swift
 Worked on by: Vi Thai, Malcolm Mckean
 Changes:
 -Initial Creation
 -Added long press button functionality
 Bugs: N/A
 Description: View Controller for activity type selection screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

class SelectActivityType: UIViewController {

    @IBOutlet weak var PhysicalActivities: UIButton!
    @IBOutlet weak var SocialActivities: UIButton!
    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var TopBackground: UIImageView!
    @IBOutlet weak var BottomBackground: UIImageView!
    
    var ActivitiesType = ActivityType.Physical
//    var CalendarEvents: [Activity] = []
    
    override func viewDidLoad() {
        PhysicalActivities.layer.cornerRadius = PhysicalActivities.frame.height * 0.5
        SocialActivities.layer.cornerRadius = SocialActivities.frame.height * 0.5
        Back.layer.cornerRadius = Back.frame.height * 0.5
        TopBackground.layer.cornerRadius = TopBackground.frame.height * 0.35
        BottomBackground.layer.cornerRadius = BottomBackground.frame.height * 0.35
        
        
        let tapGestureBack = UITapGestureRecognizer(target: self, action: #selector(tapBack))
        let longGestureBack = UILongPressGestureRecognizer(target: self, action: #selector(longBack))
        let tapGesturePhysical = UITapGestureRecognizer(target: self, action: #selector(tapPhysical))
        let longGesturePhysical = UILongPressGestureRecognizer(target: self, action: #selector(longPhysical))
        let tapGestureSocial = UITapGestureRecognizer(target: self, action: #selector(tapSocial))
        let longGestureSocial = UILongPressGestureRecognizer(target: self, action: #selector(longSocial))
        
        Back.addGestureRecognizer(tapGestureBack)
        Back.addGestureRecognizer(longGestureBack)
    PhysicalActivities.addGestureRecognizer(tapGesturePhysical)
    PhysicalActivities.addGestureRecognizer(longGesturePhysical)
        SocialActivities.addGestureRecognizer(tapGestureSocial)
        SocialActivities.addGestureRecognizer(longGestureSocial)
        
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @objc func tapBack(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "SegueToCalender", sender: nil)
        }
    }
    
    @objc func longBack(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "SegueToCalender", sender: nil)
        }
        else
        {
            
        }
    }
    
    @objc func tapPhysical(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            physicalActivitySelected()
        }
    }
    
    @objc func longPhysical(){
        if(CurrentUser.LongPress)
        {
            physicalActivitySelected()
        }
        else
        {
            
        }
    }
    
    @objc func tapSocial(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            socialActivitySelected()
        }
    }
    
    @objc func longSocial(){
        if(CurrentUser.LongPress)
        {
            socialActivitySelected()
        }
        else
        {
            
        }
    }
    
    func socialActivitySelected() {
        ActivitiesType = ActivityType.Social
        performSegue(withIdentifier: "typeSelectedSegue", sender: self)
    }
    
    func physicalActivitySelected() {
        ActivitiesType = ActivityType.Physical
        performSegue(withIdentifier: "typeSelectedSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectActivityVC = segue.destination as? SelectActivityScreenController {
            selectActivityVC.ActivitiesType = ActivitiesType
//            selectActivityVC.CalendarEvents = CalendarEvents
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
