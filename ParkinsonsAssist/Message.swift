/*
 Messages.swift
 Worked on by: Ramish Khan
 Changes:
 -Initial Creation
 -Minor UI changes
 Bugs: N/A
 Description: Main file for the messaging screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

class Message: UIViewController {
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var PageUp: UIButton!
    @IBOutlet weak var Background: UIImageView!
    @IBOutlet weak var ContactNameLabel: UILabel!
    @IBOutlet weak var PageDown: UIButton!
    @IBOutlet weak var ToggleContactTrusted: UIButton!
    @IBOutlet weak var BackgroundBottom: UIImageView!
    @IBOutlet weak var MessageText: UITextField!
    @IBOutlet weak var SendMessageButton: UIButton!
    @IBOutlet weak var Message1: UITextField!
    @IBOutlet weak var Message2: UITextField!
    @IBOutlet weak var Message3: UITextField!
    @IBOutlet weak var Message4: UITextField!
    
    var MessageTextArray : [UITextField] = []
    var ContactName : String = ""
    var db : Firestore!
    var MessageArray : [(message:String, sent:Bool)] = []
    
    var PageNum : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BackButton.layer.cornerRadius = BackButton.frame.height * 0.5
        Background.layer.cornerRadius = Background.frame.height * 0.35
        PageUp.layer.cornerRadius = PageUp.frame.height * 0.5
        PageDown.layer.cornerRadius = PageDown.frame.height * 0.5
        ToggleContactTrusted.layer.cornerRadius = ToggleContactTrusted.frame.height * 0.5
        SendMessageButton.layer.cornerRadius = SendMessageButton.frame.height * 0.5
        let tapScreen = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        //Contact roudned corners
        db = Firestore.firestore()

        view.addGestureRecognizer(tapScreen)
        
        
        ContactNameLabel.text = ContactName
        // Do any additional setup after loading the view.
        for text in MessageTextArray{
            text.isUserInteractionEnabled = false
        }
        HelperUI.disableUIButton(Button: PageUp)
        HelperUI.disableUIButton(Button: PageDown)
        Refresh()
    }
    
    func Refresh(){
        MessageTextArray = [Message1, Message2, Message3, Message4]
        let LastNameOnListWithStar = ContactNameLabel.text
        var Index = LastNameOnListWithStar!.index(LastNameOnListWithStar!.endIndex, offsetBy: 0)
        var Name = String(LastNameOnListWithStar![..<Index])
        
        if LastNameOnListWithStar?.firstIndex(of: "⭑") != nil{
            Index = LastNameOnListWithStar!.index(LastNameOnListWithStar!.endIndex, offsetBy: -1)
            Name = String(LastNameOnListWithStar![..<Index])
        }
        print(Name)
    CurrentUser.FirebaseDocRef?.collection("messages").document(Name).collection("MessageArchive").addSnapshotListener() {(MessageQuery, err) in
            if let err = err {
                print("Error getting contacts in Message.Refresh: \(err)")
            }
            else{
                self.MessageArray.removeAll()
                if let MessageSnap = MessageQuery{
                    print("h")
                    print(MessageSnap.count)
                    for message in MessageSnap.documents{
                        let MessageFromDB = message.data()["Message"] as? String
                        let SentFromDB = message.data()["Sent"] as? Bool
                        print(MessageFromDB)
                        print(SentFromDB)
                        self.MessageArray.append((MessageFromDB!, SentFromDB!))
                        
                    }
                }
            }
            var index = 1
            for message in self.MessageArray{
                self.MessageTextArray[index].backgroundColor = message.sent == true ? #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1) : #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                self.MessageTextArray[index].textAlignment = message.sent == true ? NSTextAlignment.right : NSTextAlignment.left
                self.MessageTextArray[index].text = message.message
                if index == 3{
                    break
                }
                index += 1
            }
        }
    }
    
    @IBAction func TouchToggletrusted(_ sender: UIButton) {
        if (ContactName.range(of: "⭑") != nil){
            ContactName = ContactName.substring(to: ContactName.index(before: ContactName.endIndex))
        }
        
        CurrentUser.FirebaseDocRef?.collection("contacts").whereField("uid", isEqualTo: ContactName).getDocuments() { (TrustedQuery, err) in
            if let err = err {
                print("Error getting contacts in Message.TouchToggletrusted: \(err)")
            }
            else{
                if let TrustedSnap = TrustedQuery {
                    for document in TrustedSnap.documents {
                        let trusted = document.data()["trusted"] as? Bool
                        if trusted == true {
                            CurrentUser.FirebaseDocRef?.collection("contacts").document(document.documentID).setData(["trusted": false], merge: true)
                            self.ContactNameLabel.text = self.ContactName
                        }
                        else{
                            CurrentUser.FirebaseDocRef?.collection("contacts").document(document.documentID).setData(["trusted": true], merge: true)
                            self.ContactNameLabel.text = self.ContactName + "⭑"
                        }
                    }
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    @IBAction func TouchSend(_ sender: Any) {
        guard let text = MessageText.text, !text.isEmpty else {
            return
        }
        let LastNameOnListWithStar = ContactNameLabel.text
        var Index = LastNameOnListWithStar!.index(LastNameOnListWithStar!.endIndex, offsetBy: 0)
        var Name = String(LastNameOnListWithStar![..<Index])
        
        if LastNameOnListWithStar?.firstIndex(of: "⭑") != nil{
            Index = LastNameOnListWithStar!.index(LastNameOnListWithStar!.endIndex, offsetBy: -1)
            Name = String(LastNameOnListWithStar![..<Index])
        }
        let MessageToSend = MessageText.text
        var NameDocID : String = ""
        
        db.collection("users").whereField("uid", isEqualTo: Name).getDocuments(){(DocIDQuery, err) in
            if let err = err {
                print("Error getting contacts in TouchSend: \(err)")
            }
            else{
                if let DocIDSnap = DocIDQuery{
                    for document in DocIDSnap.documents{
                        NameDocID = document.documentID
                    }
                    CurrentUser.FirebaseDocRef!.collection("messages").document(Name).collection("MessageArchive").addDocument(data: ["Message" : MessageToSend, "Sent" : true, "TimeSent" : Date()])
                    self.db.collection("users").document(NameDocID).collection("messages").document(CurrentUser.Username).collection("MessageArchive").addDocument(data: ["Message" : MessageToSend, "Sent" : false, "TimeSent" : Date()])
                }
            }
        }
        
        MessageText.text = ""
    }
}
