/*
 CalendarViewCell.swift
 Worked on by: Matthew Marinets, Vi Thai
 Changes:
 -Initial Creation
 -Reimplmented
 Bugs: N/A
 Description: This class inherits the built-in UICollectionViewCell and acts as a cell for our Calendar Collection View on Health Screen.
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

class CalendarViewCell: UICollectionViewCell {
    
    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var Icon: UILabel!
    
}
