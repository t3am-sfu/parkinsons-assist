/*
 UserDB.swift
 Worked on by: Vi Thai, Malcolm Mckean
 Changes:
 -Initial Creation
 -Updated to deal with test account
 Bugs: N/A
 Description: Class used to store different users data locally.
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import Foundation

/*The class that will store the user data*/
class UserDB
{
    var Users = ["master" : User(username: "master", password: "password")]
    
    /*Called when adding user*/
    func add(newUser: User){
        let uid = newUser.getUsername()
        if !isExist(checkUid: uid) {
            Users[uid] = newUser
        }
    }
    
    /*Checks existence of user*/
    func isExist(checkUid: String) -> Bool{
        return Users[checkUid] != nil
    }
    
    /*Gets User*/
    func getUser(with uid: String) -> User?{
        if isExist(checkUid: uid) {
            return Users[uid]
        } else {
            return nil
        }
    }
}
