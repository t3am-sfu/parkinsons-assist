/*
 Messages.swift
 Worked on by: Cooper, Malcolm
 Changes:
 -Initial Creation
 -UI changes
 -Added functionality for long button presses
 Bugs: N/A
 Description: Main file for the messaging screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */
import UIKit
import FirebaseCore
import FirebaseFirestore

class Search: UIViewController {
    
    @IBOutlet weak var TopBackground: UIImageView!
    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var Contact: UIButton!
    @IBOutlet weak var SearchButton: UIButton!
    @IBOutlet weak var NameToSearch: UITextField!
    @IBOutlet weak var CannotFindUser: UILabel!
    @IBOutlet weak var BottomBackground: UIImageView!
    @IBOutlet weak var TrustedToggle: UISwitch!
    var db: Firestore!
    var longGestureSearch : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longSearch))
        var longGestureContact : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longContact))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        db = Firestore.firestore()
        
        Back.layer.cornerRadius = Back.frame.height * 0.5
        TopBackground.layer.cornerRadius = TopBackground.frame.height * 0.35
        BottomBackground.layer.cornerRadius = BottomBackground.frame.height * 0.5
        SearchButton.layer.cornerRadius = SearchButton.frame.height * 0.5
        
        //Contact roudned corners
        Contact.layer.cornerRadius = Contact.frame.height * 0.5
        HelperUI.disableUIButton(Button: Contact)
        // Do any additional setup after loading the view.
        
        let tapScreen = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        let tapGestureBack = UITapGestureRecognizer(target: self, action: #selector(tapBack))
        let longGestureBack = UILongPressGestureRecognizer(target: self, action: #selector(longBack))
        let tapGestureSearch = UITapGestureRecognizer(target: self, action: #selector(tapSearch))
        longGestureSearch = UILongPressGestureRecognizer(target: self, action: #selector(longSearch))
        let tapGestureContact = UITapGestureRecognizer(target: self, action: #selector(tapContact))
        longGestureContact = UILongPressGestureRecognizer(target: self, action: #selector(longContact))
        
        Back.addGestureRecognizer(tapGestureBack)
        Back.addGestureRecognizer(longGestureBack)
        SearchButton.addGestureRecognizer(tapGestureSearch)
        SearchButton.addGestureRecognizer(longGestureSearch)
        Contact.addGestureRecognizer(tapGestureContact)
        Contact.addGestureRecognizer(longGestureContact)
        view.addGestureRecognizer(tapScreen)
    }
    
    @objc func tapContact(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            AddContact()
        }
    }
    
    @objc func longContact(){
        if(CurrentUser.LongPress && (longGestureContact.state == UILongPressGestureRecognizer.State.began))
        {
            AddContact()
        }
        else
        {
            
        }
    }
    
    @objc func tapBack(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "SearchToMessages", sender: self)
        }
    }
    
    @objc func longBack(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "SearchToMessages", sender: self)
        }
        else
        {
            
        }
    }
    
    @objc func tapSearch(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            PressSearch()
        }
    }
    
    @objc func longSearch(){
        if(CurrentUser.LongPress && (longGestureSearch.state == UILongPressGestureRecognizer.State.began))
        {
            PressSearch()
        }
        else
        {
            
        }
    }
    
    func PressSearch() {
        guard let TempText = NameToSearch.text, !TempText.isEmpty else {
            CannotFindUser.text = "Search field empty"
            return
        }
        db.collection("users").whereField("uid", isEqualTo: NameToSearch.text!).limit(to: 1).getDocuments() { (QuerySnapshot, err) in
            if let err = err {
                print("Error getting users in Search.PressSearch: \(err)")
            }
            else {
                if (QuerySnapshot?.isEmpty)!{
                    self.CannotFindUser.text = "Cannot find user with that ID"
                    return
                }
                else{
                    self.CannotFindUser.text = ""
                    HelperUI.enableUIButton(Button: self.Contact)
                    if let snap = QuerySnapshot{
                        for docment in snap.documents{
                            self.Contact.setTitle(docment.data()["uid"] as? String, for: .normal)
                        }
                    }
                }
            }
        }
    }
    
    func AddContact() {
        let ContactToAdd = Contact?.titleLabel?.text
        CurrentUser.FirebaseDocRef?.collection("contacts").whereField("uid", isEqualTo: ContactToAdd!).limit(to: 1).getDocuments() {(QuerySnapshot, err) in
            if let err = err{
                print("Error getting users in Search.AddContact: \(err)")
            }
            else{
                if(QuerySnapshot?.isEmpty)!{
                    var MakeTrusted : Bool = false
                    if self.TrustedToggle.isOn{
                        MakeTrusted = true
                    }
                    else{
                        MakeTrusted = false
                    }
                   
                    
                    CurrentUser.FirebaseDocRef?.collection("contacts").document(ContactToAdd!).setData(["uid" : ContactToAdd!, "name" : ContactToAdd!, "trusted" : MakeTrusted])
                    CurrentUser.FirebaseDocRef?.collection("messages").document(ContactToAdd!).setData(["uid" : ContactToAdd!, "LastMessageTime" : Date(timeIntervalSince1970: 0)])
                    self.performSegue(withIdentifier: "SearchToMessages", sender: self)
                }
                else{
                    self.CannotFindUser.text = "Contact already added"
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
