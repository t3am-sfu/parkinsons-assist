**Project Overview**

Parkinson’s Assist: a messaging app that’s got your back when you suffer an attack. Parkinson’s Assist allows you to message and organize events through your iOS device with other people with Parkinson’s disease or others who just want to help. The app also allows friends to monitor your movement, and sends a message if you are suffering from an attack and need assistance.  

Our app has three components: a general forum where users can learn about events and make event plans; a private messaging system to communicate with friends you trust; and an attack monitor that can notify people of an attack. The attack monitor can be turned off for such purposes as exercise, or dismissed within a minute in case the situation does not warrant a message.

Our iOS app will be organized to make it as easy to use as possible for users with motor impairment — buttons will be large, and will be triggered by holding a finger against them long enough for the user’s intent to be clear through any shaking or noise. Upon entering the application, the user may enter either a screen for any of our three major components.

Project Website - https://sites.google.com/view/cmpt275group3/home