/*
 ParkinsonsAssistTests.swift
 Worked on by: Matthew Marinets
 Changes:
 -Initial Creation
 Bugs: N/A
 Description: File for tests
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import XCTest
@testable import ParkinsonsAssist

/*Class where the test cases will be written*/
class ParkinsonsAssistTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
